# Crossbar Widgets

The code in this repository is intended to demonstrate how to write a Vue.js based application
that utilises Autobahn/JS and present the result as a Widget (or packaged application) that can
play well with other web pages or online applications.

For a write-up / more detailed documentation, please see
[My Blog posting](https://gareth.bult.co.uk/2019/04/02/when-is-a-widget-not-a-widget/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

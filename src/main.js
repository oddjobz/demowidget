import Vue from 'vue'
import Buefy from 'buefy'
import App from './App.vue'
import store from './store'
import vueCustomElement from 'vue-custom-element'
import 'document-register-element/build/document-register-element'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faHome, faBinoculars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faCoffee)
library.add(faHome)
library.add(faBinoculars)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

Vue.use(Buefy)
Vue.use(vueCustomElement)
App.store = store
Vue.customElement('vue-widget', App)

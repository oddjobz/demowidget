import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'

Vue.use(Vuex)

const bounce = 25

export default new Vuex.Store({
  state: {
    sessions: {}
  },
  getters: {
    get_sessions: state => state.sessions
  },
  actions: {
    add_session ({ commit }, data) { _.debounce(() => { commit('add_session', data) }, bounce)() },
    del_session ({ commit }, data) { _.debounce(() => { commit('del_session', data) }, bounce)() }
  },
  mutations: {
    add_session (state, data) { Vue.set(state.sessions, data.id, data.session) },
    del_session (state, data) { Vue.delete(state.sessions, data.id) }
  },
  strict: false
})

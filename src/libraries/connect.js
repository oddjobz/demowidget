import autobahn from 'autobahn-browser/autobahn.js'
import Logger from './logger.js'

class Connection {
  constructor (context) {
    this._context = context
    this._log = new Logger.Logger()
    this._log.info(`Autobahn version "${autobahn.version}"`)
    this._buffered = true
    this._subscriptions = {}
  }

  toastSuccess (msg) {
    this._context.$toast.open({ duration: 3000, message: msg, position: 'is-bottom-right', type: 'is-success' })
  }
  toastFailure (msg) {
    this._context.$toast.open({ duration: 5000, message: msg, position: 'is-top', type: 'is-danger' })
  }
  toastWarning (msg) {
    this._context.$toast.open({ duration: 5000, message: msg, position: 'is-top', type: 'is-warning' })
  }

  success (msg) {
    this._context.$toast.open({
      duration: 5000,
      message: msg,
      position: 'is-bottom-right',
      type: 'is-success',
      queue: false
    })
  }

  failure (msg, error) {
    this._log.error(error.args[0])
    this._context.$snackbar.open({
      message: `${msg}, error: (${error.args[0]})`,
      type: 'is-danger',
      indefinite: true
    })
  }

  initiate (profile, callback) {
    let self = this
    self._config = profile
    this.connect(profile, callback, () => {
      self._log.error('connection failed')
      self.toastFailure('Failed to connect to server')
    })
  }

  connect (profile, success, failure) {
    let self = this
    self._success = success
    self._failure = failure
    self._session = null

    function go (code) {
      self._connection = new autobahn.Connection(self._config)
      self._connection['_max_retries'] = 0
      self._connection.onopen = open
      self._connection.onclose = close
      self._connection.open()
    }
    function open (session, details) {
      self._log.info(`Session open in realm (${details.realm}) for ${details.authid} with id=${session._id}`)
      self._session = session
      if (self._success) self._success()
      self.toastSuccess('Connected to Server')
    }
    function close (reason, details) {
      self._log.info(`Connection closed :: ${reason} :: ${details.reason}`)
      switch (reason) {
        case 'lost':
          self.toastWarning('Connection to server lost!')
          return
        case 'closed':
          break
        case 'unreachable':
          self.toastWarning('Server is currently unreachable!')
          break
        case 'unsupported':
          self.toastWarning('Operation unsupported - call helpdesk!')
          break
      }
      switch (details.reason) {
        case 'wamp.close.normal':
          break
        default:
          if (details.reason) self._log.info(details.reason)
          return self._failure(reason, details)
      }
    }
    go('')
  }
  disconnect () {
    if (this._config) {
      this._log.info(`Disconnected from realm "${this._config.realm}"`)
    }
    this.close()
  }
  close () {
    if (this._connection) {
      try {
        this._connection.close()
      } catch (err) {
        this._log.info(`Error closing connection "${err}"`)
      }
    }
    this._connection = null
    this._session = null
  }
  call (topic, args, success, failure) {
    try {
      this._log.debug(`Call ${topic}(${args})`)
      if (!args) return this._session.call(topic).then(success, failure)
      return this._session.call(topic, args).then(success, failure)
    } catch (err) {
      this._log.error(`${err} tring to call [${topic}]`)
    }
  }
  unsubscribe (id, success, failure) {
    this._session.unsubscribe(this._subscriptions[id]).then(success, failure)
    delete this._subscriptions[id]
  }
  subscribe (topic, handler, options = {}, success = null, failure = null) {
    let self = this
    let queue = []
    let timer = null

    let wrapper = function (args, kwargs, details) {
      if (!self._buffered) {
        return handler(args, kwargs, details)
      }
      queue.push([args, kwargs])
      if (!timer) {
        timer = setInterval(function () {
          self._log.debug(`${self._buffered} :: Ping: ${topic} :: ${queue.length}`)
          if (self._buffered) return
          for (let event of queue) {
            handler(...event)
          }
          queue = []
          clearInterval(timer)
        }, 1000)
      }
    }
    try {
      let status = this._session.subscribe(topic, wrapper, options).then(
        function (subscription) {
          self._subscriptions[subscription.id] = subscription
          if (success) success(subscription)
        },
        function (error) { if (failure) failure(error) }
      )
      return status
    } catch (err) {
      this._log.error(`Failed to subscribe to (${topic})`)
      console.log(err)
      return false
    }
  }
}

export default { Connection }
